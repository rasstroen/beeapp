<?php
error_reporting(E_ALL);
ini_set('display_errors', true);
require_once __DIR__ . '/../vendor/autoload.php';


$application = new \bee\Application(
    require_once __DIR__ . '/../conf/main.php'
);